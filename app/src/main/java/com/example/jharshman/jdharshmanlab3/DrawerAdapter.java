package com.example.jharshman.jdharshmanlab3;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by jharshman on 10/19/15.
 */

public class DrawerAdapter extends ArrayAdapter<DrawerItem> {

    int mResourceId;
    Context mContext;
    ArrayList<DrawerItem> mList;

    DrawerAdapter(Context pContext, int pResourceId, ArrayList<DrawerItem> pList) {
        super(pContext,pResourceId,pList);
        mContext = pContext;
        mResourceId = pResourceId;
        mList = pList;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        if(convertView == null) {
            //inflate
            LayoutInflater layoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.nav_list_row, null);
        }
        //get a handle on the image and text views
        TextView txtView = (TextView)convertView.findViewById(R.id.textView);
        ImageView imgView = (ImageView) convertView.findViewById(R.id.imageView2);
        //get the object from list based on the passed in position
        DrawerItem temp = mList.get(pos);
        txtView.setText(temp.getText());
        imgView.setImageResource(temp.getResourceIconId());

        return convertView;
    }

}
