package com.example.jharshman.jdharshmanlab3;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements ListView.OnItemClickListener {

    private DrawerAdapter mAdapter;
    private ListView mListView;
    private DrawerLayout mDrawerLayout;
    private ArrayList<DrawerItem> mList;
    private ActionBarDrawerToggle mDrawerToggle;

    private final String mClosed = "JDHarshmanlab3";
    private final String mOpen = "Select A page";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mListView = (ListView)findViewById(R.id.left_drawer);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.toolbar_open, R.string.toolbar_close) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                setTitle(mClosed);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                setTitle(mOpen);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDrawerToggle.syncState();


        mList = new ArrayList<DrawerItem>();
        DrawerItem cindy = new DrawerItem(R.drawable.logo, "Cindy", R.drawable.cindy);
        DrawerItem fred = new DrawerItem(R.drawable.logo, "Fred", R.drawable.fred);
        DrawerItem kate = new DrawerItem(R.drawable.logo, "Kate", R.drawable.kate);
        DrawerItem keith = new DrawerItem(R.drawable.logo, "Keith", R.drawable.keith);
        DrawerItem matt = new DrawerItem(R.drawable.logo, "Matt", R.drawable.matt);
        DrawerItem rickey = new DrawerItem(R.drawable.logo, "Rickey", R.drawable.rickey);

        mList.add(cindy);
        mList.add(fred);
        mList.add(kate);
        mList.add(keith);
        mList.add(matt);
        mList.add(rickey);

        mAdapter = new DrawerAdapter(this, R.id.left_drawer, mList);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            aboutMe();
        }

        return super.onOptionsItemSelected(item);
    }

    public void aboutMe() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.attr_dialog_title)
                .setItems(R.array.about_me, new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                });

        builder.create();
        builder.show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ImageView imgView = (ImageView)findViewById(R.id.imageView);
        DrawerItem temp = (DrawerItem)parent.getItemAtPosition(position);
        imgView.setImageResource(temp.getPhotoId());
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if(mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            //drawer is open
            menu.findItem(R.id.action_settings).setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }
}
