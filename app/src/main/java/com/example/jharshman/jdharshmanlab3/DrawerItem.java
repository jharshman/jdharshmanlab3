package com.example.jharshman.jdharshmanlab3;

/**
 * Created by jharshman on 10/19/15.
 */
public class DrawerItem {

    private int mPhoto;
    private int mResourceIcon;
    private String mText;

    public DrawerItem(int pResourceIcon, String pText, int pPhoto) {
        mResourceIcon = pResourceIcon;
        mText = pText;
        mPhoto = pPhoto;
    }

    public int getResourceIconId() {
        return mResourceIcon;
    }

    public String getText() {
        return mText;
    }

    public int getPhotoId() {
        return mPhoto;
    }

}
